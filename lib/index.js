'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

exports.default = function () {
  var twingleContainers = document.getElementsByClassName('twingle-container');

  if (twingleContainers.length) {
    for (var i = 0; i < twingleContainers.length; i++) {
      var container = twingleContainers.item(i);
      var formUrl = container.getAttribute('data-url');
      var urlParams = new URLSearchParams(window.location.search);
      var urlTwingleId = urlParams.get("tid");

      if (urlTwingleId) {
        formUrl = formUrl.replace(/\/([a-z0-9]+)\/widget/g, '/' + urlTwingleId + '/widget');
      }

      var domId = '_' + Math.random().toString(36).substr(2, 9);
      var d = document,
          g = d.createElement('script'),
          s = d.getElementsByTagName('script')[0];
      container.setAttribute('id', 'twingle-public-embed-' + domId);
      g.type = 'text/javascript';
      g.async = true;
      g.defer = true;
      g.src = formUrl + '/' + domId;
      s.parentNode.insertBefore(g, s);
    };

    window.addEventListener('message', function (event) {
      if (event && event.data && event.data.type === 'donationFinished') {
        if (Array.isArray(window.dataLayer)) {
          window.dataLayer.push({
            event: 'donation:twingle',
            twinglePaymentType: event.data.value.paymentMethod,
            twingleAmount: event.data.value.amount,
            twingleRythm: event.data.value.rythm,
            twingleRecurringRythm: event.data.value.recurringRythm
          });
        }
      }
    }, false);
  }
};