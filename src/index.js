export default () => {
  const twingleContainers = document.getElementsByClassName('twingle-container');

  if (twingleContainers.length) {
    for (let i = 0; i < twingleContainers.length; i++) {
      const container = twingleContainers.item(i);
      let formUrl = container.getAttribute('data-url');
      const urlParams = new URLSearchParams(window.location.search);
      const urlTwingleId = urlParams.get("tid");

      if (urlTwingleId) {
        formUrl = formUrl.replace(/\/([a-z0-9]+)\/widget/g, '/' + urlTwingleId + '/widget');
      }

      const domId = '_' + Math.random().toString(36).substr(2, 9);
      const d = document, g = d.createElement('script'), s = d.getElementsByTagName('script')[0];
      container.setAttribute('id', 'twingle-public-embed-' + domId);
      g.type = 'text/javascript';
      g.async = true;
      g.defer = true;
      g.src = formUrl + '/' + domId;
      s.parentNode.insertBefore(g, s);
    };

    window.addEventListener('message', function(event) {
      if (event && event.data && event.data.type === 'donationFinished') {
        if (Array.isArray(window.dataLayer)) {
          window.dataLayer.push({
            event: 'donation:twingle',
            twinglePaymentType: event.data.value.paymentMethod,
            twingleAmount: event.data.value.amount,
            twingleRythm: event.data.value.rythm,
            twingleRecurringRythm: event.data.value.recurringRythm
          });
        }
      }
    }, false);
  }
};